package test;

import java.util.Scanner;

public class Fibonachi {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int firstPoint;
        int lastPoint;
        int size;
        System.out.println("Input the Interval firstPoint: ");
        firstPoint = in.nextInt();
        System.out.println("Input the Interval lastPoint: ");
        lastPoint = in.nextInt();
        printIntervalNumbers(firstPoint, lastPoint);
        System.out.println("Input size of Fibonachi numbers: ");
        size = in.nextInt();
        printFibonachiNumbers(size);
    }

    public Fibonachi() {
    }

    public static void printIntervalNumbers(final int firstPoint, final int lastPoint) {

        int sumOfOddNumbers = 0;
        int sumOfEvenNumbers = 0;
        System.out.println("Odd numbers \n");
        for (int i = firstPoint; i <= lastPoint; i++) {
            if (!checkOddOrEven(i)) {
                System.out.print(i + " ");
                sumOfOddNumbers += i;
            }
        }
        System.out.println("Even numbers \n");
        for (int i = lastPoint; i >= firstPoint; i--) {
            if (checkOddOrEven(i)) {
                System.out.print(i + " ");
                sumOfEvenNumbers += i;
            }
        }
        System.out.println("\nSum of odd numbers is:" + sumOfOddNumbers);
        System.out.println("Sum of even numbers is: " + sumOfEvenNumbers);
    }

    public static void printFibonachiNumbers(final int size) {
        int n0 = 1;
        int n1 = 1;
        int n2;
        int biggestOddNumber = n0;
        int biggestEvenNumber = n1;
        int countOfOddNumbers = 0;
        int countOfEvenNumbers = 0;
        double onePercent = 0.0;
        double percentOfOddNumbers = 0.0;
        double percentOfEvenNumbers = 0.0;
        double oneHundredPercent = 100;
        System.out.print(n0 + " " + n1 + " ");
        for (int i = 2; i < size; i++) {
            n2 = n0 + n1;
            System.out.print(n2 + " ");
            n0 = n1;
            n1 = n2;
            if (checkOddOrEven(n2)) {
                if (biggestEvenNumber < n2) {
                    biggestEvenNumber = n2;
                }
                countOfEvenNumbers++;
            } else {
                if (biggestOddNumber < n2) {
                    biggestOddNumber = n2;
                }
                countOfOddNumbers++;
            }
        }
        System.out.println("\nBiggest odd number is " + biggestOddNumber);
        System.out.println(("Biggest even number is " + biggestEvenNumber));
        onePercent = (countOfEvenNumbers + countOfOddNumbers) / oneHundredPercent;
        percentOfEvenNumbers = java.lang.Math.round(countOfEvenNumbers / onePercent);
        percentOfOddNumbers = java.lang.Math.round(countOfOddNumbers / onePercent);
        System.out.println("Percentage of odds number is " + percentOfOddNumbers);
        System.out.println("Percentage of evens number is " + percentOfEvenNumbers);
    }

    public static boolean checkOddOrEven(final int number) {
        return (number % 2 == 0);
    }
}
